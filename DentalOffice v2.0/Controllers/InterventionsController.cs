﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DentalOffice_v2._0.App_Start;
using DentalOffice_v2._0.Models;

namespace DentalOffice_v2._0.Controllers
{
    [AuthFilter]
    public class InterventionsController : Controller
    {
        private DentalOfficeEntities db = new DentalOfficeEntities();

        // GET: Interventions
        public ActionResult Index()
        {
            var interventions = db.Interventions.Include(i => i.Doctor).Include(i => i.Nurse).Include(i => i.PatientFile).Include(i => i.Service);
            return View(interventions.ToList());
        }

        // GET: Interventions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = db.Interventions.Find(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            return View(intervention);
        }

        // GET: Interventions/Create
        public ActionResult Create()
        {
            ViewBag.doctor_id = new SelectList(db.Doctors, "doctor_id", "first_name");
            ViewBag.nurse_id = new SelectList(db.Nurses, "nurse_id", "first_name");
            //ViewBag.patientfile_id = new SelectList(db.PatientFiles, "patientfile_id", "");
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name");
            return View();
        }

        // POST: Interventions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "intervention_id,service_id,patientfile_id,doctor_id,nurse_id")] Intervention intervention)
        {
            if (ModelState.IsValid)
            {
                db.Interventions.Add(intervention);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.doctor_id = new SelectList(db.Doctors, "doctor_id", "first_name", intervention.doctor_id);
            ViewBag.nurse_id = new SelectList(db.Nurses, "nurse_id", "first_name", intervention.nurse_id);
            ViewBag.patientfile_id = new SelectList(db.PatientFiles, "patientfile_id", "time", intervention.patientfile_id);
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name", intervention.service_id);
            return View(intervention);
        }

        // GET: Interventions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = db.Interventions.Find(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            ViewBag.doctor_id = new SelectList(db.Doctors, "doctor_id", "first_name", intervention.doctor_id);
            ViewBag.nurse_id = new SelectList(db.Nurses, "nurse_id", "first_name", intervention.nurse_id);
            ViewBag.patientfile_id = new SelectList(db.PatientFiles, "patientfile_id", "time", intervention.patientfile_id);
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name", intervention.service_id);
            return View(intervention);
        }

        // POST: Interventions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "intervention_id,service_id,patientfile_id,doctor_id,nurse_id")] Intervention intervention)
        {
            if (ModelState.IsValid)
            {
                db.Entry(intervention).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.doctor_id = new SelectList(db.Doctors, "doctor_id", "first_name", intervention.doctor_id);
            ViewBag.nurse_id = new SelectList(db.Nurses, "nurse_id", "first_name", intervention.nurse_id);
            ViewBag.patientfile_id = new SelectList(db.PatientFiles, "patientfile_id", "time", intervention.patientfile_id);
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name", intervention.service_id);
            return View(intervention);
        }

        // GET: Interventions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = db.Interventions.Find(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            return View(intervention);
        }

        // POST: Interventions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Intervention intervention = db.Interventions.Find(id);
            db.Interventions.Remove(intervention);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
