﻿using DentalOffice_v2._0.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DentalOffice_v2._0.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private DentalOfficeEntities db = new DentalOfficeEntities();
        public string url = "";
        public string msg = "";

        // GET: Login
        
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignIn([Bind(Include = "username,password")] Nurse nurse) 
        {

            
            foreach (var n in db.Nurses.ToList())
            {
                if(nurse.username.Equals(n.username.Trim()) && nurse.password.Equals(n.password.Trim()))
                {
                    Session["nurse"] = n;
                    this.url = "/Home/Index";
                    break;
                }
                else
                {
                    TempData["msg"] = "Invalid login info!";
                    this.url = "/Login/Index";
                }
                       
            }
            return Redirect(this.url);
        }

        public ActionResult LogOut()
        {

            Session["nurse"] = null;

            return RedirectToAction("Index");
        }
    }
}