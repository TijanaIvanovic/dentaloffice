﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DentalOffice_v2._0.App_Start;
using DentalOffice_v2._0.Models;

namespace DentalOffice_v2._0.Controllers
{
    [AuthFilter]
    public class Bill_ItemController : Controller
    {
        private DentalOfficeEntities db = new DentalOfficeEntities();
        public string Msg { get; set; }

        // GET: Bill_Item
        public ActionResult Index()
        {
            
            var bill_Item = db.Bill_Item.Include(b => b.Bill).Include(b => b.Service);
            return View(bill_Item.ToList());
        }

        // GET: Bill_Item/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill_Item bill_Item = db.Bill_Item.Find(id);
            if (bill_Item == null)
            {
                return HttpNotFound();
            }
            return View(bill_Item);
        }

        // GET: Bill_Item/Create
        public ActionResult Create(int id)
        {
            if (id == null)
            {
                return Redirect("/Bills/Create");
            }
            ViewBag.bill_id =  id;
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name");
            return View();
        }

        // POST: Bill_Item/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "bill_item_id,service_id,bill_id,quantity")] Bill_Item bill_Item, int id)
        {
            if (ModelState.IsValid)
            {
                bill_Item.bill_id = id;

                db.Bill_Item.Add(bill_Item);
                db.SaveChanges();
                

                return Redirect("/InfoMessage/Index/" + "Item added succesfully");
            }

            ViewBag.bill_id = new SelectList(db.Bills, "bill_id", "bill_id", bill_Item.bill_id);
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name", bill_Item.service_id);
            return View(bill_Item);
        }

        // GET: Bill_Item/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill_Item bill_Item = db.Bill_Item.Find(id);
            if (bill_Item == null)
            {
                return HttpNotFound();
            }
            ViewBag.bill_id = new SelectList(db.Bills, "bill_id", "bill_id", bill_Item.bill_id);
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name", bill_Item.service_id);
            return View(bill_Item);
        }

        // POST: Bill_Item/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "bill_item_id,service_id,bill_id,quantity")] Bill_Item bill_Item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bill_Item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.bill_id = new SelectList(db.Bills, "bill_id", "bill_id", bill_Item.bill_id);
            ViewBag.service_id = new SelectList(db.Services, "service_id", "name", bill_Item.service_id);
            return View(bill_Item);
        }

        // GET: Bill_Item/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill_Item bill_Item = db.Bill_Item.Find(id);
            if (bill_Item == null)
            {
                return HttpNotFound();
            }
            return View(bill_Item);
        }

        // POST: Bill_Item/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bill_Item bill_Item = db.Bill_Item.Find(id);
            db.Bill_Item.Remove(bill_Item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
