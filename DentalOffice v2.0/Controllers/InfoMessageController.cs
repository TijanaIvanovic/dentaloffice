﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalOffice_v2._0.App_Start;
using DentalOffice_v2._0.Models;

namespace DentalOffice_v2._0.Controllers
{
    [AuthFilter]
    public class InfoMessageController : Controller
    {

        

        // GET: InfoMessage
        public ActionResult Index(string id)
        {
            InfoMessage message = new InfoMessage(id);
            
            return View(message);
        }
    }
}