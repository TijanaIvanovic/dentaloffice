﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalOffice_v2._0.App_Start;
using DentalOffice_v2._0.Models;

namespace DentalOffice_v2._0.Controllers
{
    public class HomeController : Controller
    {
        [AuthFilter]
        public ActionResult Index()
        {
            if(Session["Nurse"]==null)
            {
                return Redirect("/Login/Index");
            }

            var nurse = (Nurse)Session["nurse"];
            return View(nurse);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}