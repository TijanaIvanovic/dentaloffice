﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DentalOffice_v2._0.App_Start;
using DentalOffice_v2._0.Models;

namespace DentalOffice_v2._0.Controllers
{
    [AuthFilter]
    public class PatientFilesController : Controller
    {
        private DentalOfficeEntities db = new DentalOfficeEntities();

        // GET: PatientFiles
        public ActionResult Index()
        {
            var patientFiles = db.PatientFiles.Include(p => p.Patient);
            return View(patientFiles.ToList());
        }

        // GET: PatientFiles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PatientFile patientFile = db.PatientFiles.Find(id);
            if (patientFile == null)
            {
                return HttpNotFound();
            }
            return View(patientFile);
        }

        // GET: PatientFiles/Create
        public ActionResult Create()
        {
            ViewBag.patient_id = new SelectList(db.Patients, "patient_id", "Full_name");
            return View();
        }

        // POST: PatientFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "patientfile_id,date,time,patient_id")] PatientFile patientFile)
        {
            if (ModelState.IsValid)
            {
                db.PatientFiles.Add(patientFile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.patient_id = new SelectList(db.Patients, "patient_id", "first_name", patientFile.patient_id);
            return View(patientFile);
        }

        // GET: PatientFiles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PatientFile patientFile = db.PatientFiles.Find(id);
            if (patientFile == null)
            {
                return HttpNotFound();
            }
            ViewBag.patient_id = new SelectList(db.Patients, "patient_id", "Full_name", patientFile.patient_id);
            return View(patientFile);
        }

        // POST: PatientFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "patientfile_id,date,time,patient_id")] PatientFile patientFile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(patientFile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.patient_id = new SelectList(db.Patients, "patient_id", "first_name", patientFile.patient_id);
            return View(patientFile);
        }

        // GET: PatientFiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PatientFile patientFile = db.PatientFiles.Find(id);
            if (patientFile == null)
            {
                return HttpNotFound();
            }
            return View(patientFile);
        }

        // POST: PatientFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PatientFile patientFile = db.PatientFiles.Find(id);
            db.PatientFiles.Remove(patientFile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
