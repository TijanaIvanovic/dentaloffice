﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DentalOffice_v2._0.App_Start
{
    public class AuthFilter: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var redirectTarget = new RouteValueDictionary() { { "action", "" }, { "controller", "" } };
            var session = filterContext.HttpContext.Session;
            
            if (session["nurse"] == null)
            {
                redirectTarget["action"] = "Index";
                redirectTarget["controller"] = "LogIn";

                filterContext.Result = new RedirectToRouteResult(redirectTarget);
            }
                
            
        }
    }
}
