﻿
$('#username').focus(function () {
    $(this).css('transform', 'scale(1.05)');
    $('#usernameLBL').css('transform', 'translateY(-50%) scale(1)');
});
$('#username').blur(function () {
    $(this).css('transform', 'scale(0.9)');
    if ($(this).val() != '') {
        $(this).css('backgroundColor', '#E8F0FE');
    } else {
        $(this).css('backgroundColor', 'transparent');
    }
    $('#usernameLBL').css('transform', 'translateY(150%) scale(0.9)');
});

$('#password').focus(function () {
    $(this).css('transform', 'scale(1.05)');
    $('#passwordLBL').css('transform', 'translateY(-50%) scale(1)');
});
$('#password').blur(function () {
    $(this).css('transform', 'scale(0.9)');
    if ($(this).val() != '') {
        $(this).css('backgroundColor', '#E8F0FE');
    } else {
        $(this).css('backgroundColor', 'transparent');
    }
    $('#passwordLBL').css('transform', 'translateY(150%) scale(0.9)');
});





// validation
function validate() {
    let user = $('input[name=username]').val();
    let pass = $('input[name=password]').val();

    console.log(user)
    //let regExUser = /^[A-Z][a-z]{2,20}$/;
    //let regExPass = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

    if (user === ''  ||  pass === '') {
        $('#error').text(
            'Enter login info!'
        ).css('opacity', '1');
        event.preventDefault();
        return false;
    }
    else {
        //$('#error').html('&nbsp;').css('opacity', '0');
        return true;
    }
}