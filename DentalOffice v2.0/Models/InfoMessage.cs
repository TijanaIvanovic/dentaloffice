﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalOffice_v2._0.Models
{
    public class InfoMessage
    {
        public string Text { get; set; }

        public InfoMessage(string text)
        {
            this.Text = text;
        }
    }
}